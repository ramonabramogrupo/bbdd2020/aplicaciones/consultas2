<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Consulta 3';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emple-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <h2>Mostrando con DataProvider</h2>
    <?= GridView::widget([
        'dataProvider' => $datos,
        'columns' => [
            'apellido',
            'oficio',
       ],
    ]); ?>
    
</div>
