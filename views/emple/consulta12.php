<?php

/* @var $this yii\web\View */
    use yii\helpers\Html;
    use yii\grid\GridView;


    $this->title = 'Consulta 12';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emple-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?= GridView::widget([
        'dataProvider' => $dataprovider,
        'columns' => [
            //campo nuevo en gridView
            [
                'label'=>'Apellidos',
                'format'=>'texto',//raw, html
                'content'=>function($data){
                    return strtoupper($data->apellido);
                }
             ],
            'emp_no',
            //'apellido',
            'oficio',
            'dir',
            'fecha_alt',
            'salario',
            'comision',
            'dept_no',
           
        ],
    ]); ?>
    
    <div>