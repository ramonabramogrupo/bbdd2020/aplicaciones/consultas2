<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Consulta1';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emple-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <h2>Mostrando con DataProvider</h2>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'emp_no',
            'apellido',
            'oficio',
            'dir',
            'fecha_alt',
            'salario',
            'comision',
            'dept_no',
        ],
    ]); ?>
    
    <h2>Utilizando ActiveQuery</h2>
    <div>
        <?php
            var_dump($datos);
        ?>
    </div>
    <h2>Utilizando QueryBuilder</h2>
        <div>
        <?php
            var_dump($listado);
        ?>
    </div>
    <h2>Utilizando DAO</h2>
        <div>
        <?php
            var_dump($consulta);
        ?>
    </div>
</div>
