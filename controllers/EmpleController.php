<?php

namespace app\controllers;

use Yii;
use app\models\Emple;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\db\Query;

/**
 * EmpleController implements the CRUD actions for Emple model.
 */
class EmpleController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Emple models.
     * @return mixed
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Emple::find(),
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Emple model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Emple model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Emple();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->emp_no]);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Emple model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->emp_no]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Emple model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Emple model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Emple the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Emple::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    // Mostrar todos los campos y todos los 
    // registros de la tabla empleado
    public function actionConsulta1() {
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        $r = Emple::find();

        /**
         * voy a utilizar un dataprovider 
         * desde activeQuery
         */
        $d = new ActiveDataProvider([
            "query" => $r,
        ]);


        /**
         * ejecuto la consulta
         * array de activeRecord (un array de modelos)
         */
        $resultado = $r->all();


        /**
         * crear una consulta con QueryBuilder
         * 
         */
        $listado = (new Query())
                ->select("*")
                ->from("emple")
                ->all();

        /**
         * Crear una consulta con el objeto conexion
         */
        $consulta = Yii::$app->db
                ->createCommand("select * from emple")
                ->queryAll();

        return $this->render("consulta1", [
                    "datos" => $resultado,
                    "dataProvider" => $d,
                    "listado" => $listado,
                    "consulta" => $consulta,
        ]);
    }

    // Mostrar el apellido y oficio de cada empleado.
    public function actionConsulta3() {

        $consulta = Emple::find()
                ->select("apellido,oficio");


        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render("consulta3", [
                    "datos" => $dp
        ]);
    }

    public function actionConsulta6() {

        $consulta = Emple::find()
                ->count();

        return $this->render("consulta6", [
                    "d" => $consulta
        ]);
    }

    public function actionConsulta7() {
        $consulta = Emple::find()
                ->orderBy('apellido');

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta7', [
                    "datos" => $dp
        ]);
    }

    public function actionConsulta8() {
        /**
         * voy a utilizar ActiveQuery
         * desde ActiveRecord
         */
        $r = Emple::find()->orderBy(["apellido" => SORT_DESC]);

        /**
         * voy a utilizar un dataprovider 
         * desde activeQuery
         */
        $d = new ActiveDataProvider([
            "query" => $r,
            "pagination" => [
                "pageSize" => 5,
            ],
        ]);

        return $this->render("consulta8", [
                    "datos" => $d,
        ]);
    }

    // Datos de los empleados ordenados por número de departamento descendentemente.
    public function actionConsulta10() {

        $consulta = Emple::find()
                ->orderBy(["dept_no" => SORT_DESC]);


        $dp = new ActiveDataProvider([
            "query" => $consulta,
            "pagination" => [
                "pageSize" => 5,
            ],
        ]);

        return $this->render("consulta10", [
                    "datos" => $dp
        ]);
    }

    public function actionConsulta11() {

        $datos = Emple::find()
                ->orderBy(['dept_no' => SORT_DESC, 'oficio' => SORT_ASC]);

        $titulo = 'Consulta 11';

        $d = new ActiveDataProvider([
            "query" => $datos,
            "pagination" => [
                'pageSize' => 5
            ]
        ]);


        $desc = 'Datos de los empleados ordenados por departamento de forma descendente y por oficio asscendente';

        return $this->render('consulta11', [
                    'datos' => $datos->all(),
                    'dp' => $d,
                    'desc' => $desc,
                    'titulo' => $titulo,
        ]);
    }

    public function actionConsulta12() {

        $consulta = Emple:: find()
                ->orderBy(['dept_no' => SORT_DESC,
            'apellido' => SORT_ASC]
        );

        $a = new ActiveDataProvider([
            "query" => $consulta,
        ]);


        return $this->render('consulta12', [
                    "dataprovider" => $a,
        ]);
    }

    public function actionConsulta13() {

        $consulta = Emple:: find()
                ->select('emp_no')
                ->where('salario>2000');

        $a = new ActiveDataProvider([
            "query" => $consulta,
            "pagination" => [
                'pageSize' => 3,
            ],
        ]);


        return $this->render('consulta13', [
                    "dataprovider" => $a,
        ]);
    }

    public function actionConsulta14() {
        $consulta = Emple::find()
                ->select('emp_no,apellido')
                ->where('salario<2000');

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta14', [
                    'titulo' => 'Consulta 14',
                    'texto' => "Mostrar los códigos y los apellidos de los empleados cuyo salario sea menor 2000",
                    'datos' => $dp
        ]);
    }

    public function actionConsulta15() {
        $consulta = Emple::find()
                ->where(['between', 'salario', 1500, 2500]);


        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta15', [
                    'titulo' => 'Consulta 15',
                    'texto' => "Mostrar los datos de los empleados cuyo salario este entre 1500 y 2500",
                    'datos' => $dp
        ]);
    }

    public function actionConsulta16() {
        $consulta = Emple::find()
                //->where("oficio='analista'");
                //->where(["oficio"=>"analista"]);
                ->where(["=", "oficio", "analista"]);


        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consulta16', [
                    'titulo' => 'Consulta 16',
                    'texto' => "Mostrar los datos de los empleados cuyo oficio sea ʻANALISTAʼ.",
                    'datos' => $dp
        ]);
    }

    // Mostrar los datos de los empleados cuyo oficio sea ANALISTA y ganen mas de 2000 €.
    public function actionConsulta17() {

        $consulta = Emple::find()
                //->where("oficio like 'analista' and salario>2000");
                ->where(['and', ['oficio' => 'ANALISTA'], ['>', 'salario', 2000]]);


        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render("consulta17", [
                    "datos" => $dp,
                    "titulo" => "Consulta 17",
                    "texto" => "Mostrar los datos de los empleados cuyo oficio sea ANALISTA y ganen mas de 2000 €."
        ]);
    }

    public function actionConsulta18() {


        $datos = Emple::find()
                ->where(['dept_no' => '20']);


        $titulo = 'Consulta 18';
        $d = new ActiveDataProvider([
            "query" => $datos
        ]);
        $desc = 'Sleeccionar apellido y oficio de los empleados del departamento 20';

        return $this->render('consultas18', [
                    'datos' => $d,
                    'texto' => $desc,
                    'titulo' => $titulo,
        ]);
    }

    public function actionConsulta19() {

        // Objeto ActiveQuery de Emple (ActiveRecord)
        $datos = Emple::find()
                ->where("oficio='VENDEDOR'");
        //->where(["oficio"=>"VENDEDOR"]);
        // string
        $titulo = 'Consulta 19';

        // numero
        $d = $datos->count();

        // string
        $desc = 'Contar el número de empleados cuyo oficio sea VENDEDOR';

        return $this->render('consultas', [
                    'resultado' => "El numero de vendedores es " . $d,
                    'texto' => $desc,
                    'titulo' => $titulo,
                    'datos' => NULL,
                    'columnas' => NULL,
        ]);
    }

    public function actionConsulta20() {

        $datos = Emple::find()
                ->where(["OR", ("apellido like 'm%'"), ("apellido like 'n%'")])
                ->orderBy("apellido");

        $titulo = 'Consulta 20';
        $d = new ActiveDataProvider([
            "query" => $datos
        ]);
        $desc = 'Mostrar todos los datos de los empleados cuyos apellidos comiencen por m o por n ordenados por apellido de forma ascendente.';

        return $this->render('consultas', [
                    'resultado' => NULL,
                    'texto' => $desc,
                    'titulo' => $titulo,
                    'datos' => $d,
                    'columnas' => NULL,
        ]);
    }

    public function actionConsulta21() {
        $consulta = Emple::find()
                ->where(['oficio' => 'VENDEDOR'])
                ->orderBy(['apellido' => SORT_ASC]);

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consultas', [
                    'resultado' => NULL,
                    'titulo' => 'Consulta 21',
                    'texto' => "Seleccionar los empleados cuyo oficio sea 'VENDEDOR'. Mostrar los datos ordenados por apellido de forma ascendente",
                    'datos' => $dp,
                    'columnas' => NULL,
        ]);
    }

    public function actionConsulta22() {

        $titulo = 'Consulta 22';

        $desc = 'Apellido del Empleado que más gana';


        $max = Emple::find()->max('salario');

        $datos = Emple::find()->select('apellido')
                ->where(['salario' => $max])
        ;

        $d = new ActiveDataProvider([
            "query" => $datos,
        ]);

        return $this->render('consultas', [
                    'resultado' => NULL,
                    'datos' => $d,
                    'texto' => $desc,
                    'titulo' => $titulo,
                    'columnas' => ['apellido'],
        ]);
    }

    public function actionConsulta23() {

        $consulta = Emple::find()
                ->where(['AND', ("dept_no=10"), ("oficio like 'ANALISTA'")
                ])
                ->orderBy(['apellido' => SORT_ASC, 'oficio' => SORT_ASC]);

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consultas', [
                    'resultado' => NULL,
                    'columnas' => NULL,
                    'datos' => $dp,
                    'texto' => "Mostrar los empleados cuyo departamento sea 10 y cuyo oficio sea ?ANALISTA'. Ordenar el resultado por apellido y
oficio de forma ascendente",
                    'titulo' => "Consulta 23",
        ]);
    }

    public function actionConsulta24() {

        $consulta = Emple:: find()->
                select('month(fecha_alt) as salida')
                ->distinct('salida');

        $a = new ActiveDataProvider([
            "query" => $consulta,
        ]);

        return $this->render('consultas', [
                    'resultado' => NULL,
                    'columnas' => ['salida'],
                    "datos" => $a,
                    'texto' => 'Listado de los distintos meses en que los empleados se han dado de alta.',
                    'titulo' => 'consulta 24',
        ]);
    }
    
        public function actionConsulta25() {

        $r=Emple::find()
            ->select('year(fecha_alt) as salida')
            ->distinct('salida')
            ->orderBy('salida');

        $a = new ActiveDataProvider([
            "query" => $r,
        ]);

        return $this->render('consultas', [
                    'resultado' => NULL,
                    'columnas' => ['salida'],
                    "datos" => $a,
                    'texto' => 'Listado de los distintos años en que los empleados se han dado de alta.',
                    'titulo' => 'consulta 25',
        ]);
    }

     public function actionConsulta26() {

        $r=Emple::find()
            ->select('day(fecha_alt) as salida')
            ->distinct('salida')
            ->orderBy('salida');

        $a = new ActiveDataProvider([
            "query" => $r,
        ]);

        return $this->render('consultas', [
                    'resultado' => NULL,
                    'columnas' => ['salida'],
                    "datos" => $a,
                    'texto' => 'Listado de los distintos días en que los empleados se han dado de alta.',
                    'titulo' => 'consulta 26',
        ]);
    }
    
    
    public function actionConsulta27() {

        $r=Emple::find()
            ->select('apellido')
            ->where(['or','salario>2000','dept_no=20']);

        $a = new ActiveDataProvider([
            "query" => $r,
        ]);

        return $this->render('consultas', [
                    'resultado' => NULL,
                    'columnas' => ['apellido'],
                    "datos" => $a,
                    'texto' => 'Mostrar los apellidos de los empleados que tengan un salario mayor que 2000 o que pertenezcan al departamento número 20',
                    'titulo' => 'consulta 27',
        ]);
    }
    
    public function actionConsulta28() {
        $consulta = Emple::find();
                //->select("apellido,dnombre")
                //->joinWith("deptNo");
                

        $dp = new ActiveDataProvider([
            "query" => $consulta
        ]);

        return $this->render('consultas', [
                    'resultado' => NULL,
                    'columnas' => ['apellido','deptNo.dnombre'],
                    'datos' => $dp,
                    'titulo' => 'Consulta 28',
                    'texto' => "Realizar un listado donde nos coloque el apellido del empleado y el nombre del departamento al que pertenece",
                    
        ]);
    }
}
